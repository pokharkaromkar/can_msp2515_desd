
#include "mcp_can.h"
#include "spi.h"
#include "uart.h"
#include "mcp_can_dfs.h"
#define PRESCALAR 1024
#include<stdio.h>
#include<avr/io.h>
void timer1_delay_ms(uint16_t ms)
{
	uint16_t cnt;
//	calculate the count
		cnt = (F_CPU / 1000) * ms / PRESCALAR;
	// set TCNT1
	TCNT1 = 65535 - cnt;
	// config timer: mode=Normal, Prescalar=1024.
	TCCR1A = 0x00;
	TCCR1B = BV(CS12) | BV(CS10);
	// wait for TOV1 flag
	while( (TIFR & BV(TOV1)) == 0 )
		;
	// clear TOV1 flag
	TIFR |= BV(TOV1);
	// stop timer
	TCCR1B = 0x00;
}
void setup()
{
	uart_init(9600);
	spi_init_master();
	PORTB=(1<<SS);
	MCP_CAN can;
	unsigned int res =can.begin(CAN_100KBPS,MCP_8MHz);
}

int main()
{
	char str[64];
	unsigned char str1[64];
	unsigned char *ptr = str1;
	uart_init(9600);
	spi_init_master();
	PORTB=(1<<SS);
	MCP_CAN can;
	sprintf(str,"CAN begin \r\n");
	uart_puts(str);
	timer1_delay_ms(100);
	unsigned int res =can.begin(CAN_100KBPS,MCP_8MHz);
	if(res == CAN_OK)
	{
		sprintf(str,"OMKAR : Successful\r\n");
		uart_puts(str);
		timer1_delay_ms(100);
	}
	if(res == CAN_FAILINIT)
	{
		sprintf(str,"OMKAR : unSuccessfull\r\n");
		uart_puts(str);
		timer1_delay_ms(100);
	}
	sprintf(str,"CAN begin ended \r\n");
	uart_puts(str);
	timer1_delay_ms(100);

//	res = can.getCanId();
//	sprintf(str," res = %d\r\n",res);
//	uart_puts(str);

	sprintf((char*)str1,"Hello 12!!");
	INT8U exFlag = 0;
	INT8U can_ID = 100;
	INT32U length = 11;
	res = can.sendMsgBuf(can_ID,exFlag,length,ptr);
	if(res == CAN_OK)
	{
		sprintf(str," sendMsgBuf successful\r\n");
		uart_puts(str);
	}
	else
	{
		sprintf(str," sendMsgBuf unsuccessful %d\r\n",res);
		uart_puts(str);
	}
	return 0;
}

/*********************************************************************************************************
  END FILE
 *********************************************************************************************************/

